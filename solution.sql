--task 1
SELECT customerName FROM customers WHERE country ='Philippines';

-- task 2

SELECT contactLastName,contactFirstName FROM customers WHERE customerName LIKE "%Rochelle%";

-- task 3
SELECT productName, MSRP FROM products WHERE productName LIKE "%titanic%";

--task 4

SELECT firstName,lastName FROM employees WHERE email='jfirrelli@classicmodelcars.com';

--task 5

SELECT customerName FROM customers WHERE state IS NULL;

-- task 6
SELECT firstName,lastName,email FROM employees WHERE lastName LIKE '%patterson%' AND firstName LIKE '%steve%';

--task 7

SELECT customerName, country,creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;

--task 8

SELECT orderNumber FROM orders WHERE comments LIKE '%dhl%';

--task 9

 SELECT productLine FROM productlines WHERE textDescription LIKE '%state_of_the_art%';

-- task 10

SELECT DISTINCT country FROM customers;

--task 11

SELECT DISTINCT status FROM orders;

--task 12

SELECT customerName, country FROM customers WHERE country IN ('USA','France','Canada');

--task 13   
SELECT firstName, lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city ='Tokyo';

--task 14

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE CONCAT(firstName,' ',lastName) = 'Leslie Thompson';

--task 15

SELECT products.productName, customers.customerName FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders
ON orderdetails.orderNumber = orders.orderNumber JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerName LIKE '%Baane_Mini_Imports%';
--task 16

SELECT employees.firstName, employees.lastName,customerName, offices.country FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber 
JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;
--task 17
SELECT productName,quantityInStock FROM products JOIN productlines ON products.productLine = productlines.productLine WHERE productLine like '%plane%';

--task 18

SELECT customerName FROM customers WHERE phone LIKE '%+81%';